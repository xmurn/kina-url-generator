# Kina tools for generating Kina Url from  JS

# How to make it shine
#### 2.  npm install
#### 3. npm run build

After  updating this lib next things needs to be checked:
- Sirius dependency
- CakeMonster core Lib

##Usages
In Browser
```
<script type="application/javascript" src="KinaTools.js"></script>
KinaTools.createUrl(parameters);
```

In Node

```
const KinaTools = require('kina-tools/KinaTools')
KinaTools.KinaTools.createUrl(parameters)
```

###Create url support
```
const actions = [
   {
      "resize":{
         "width":35,
         "height":45,
         "aspectRatio":true,
         "minSize":true
      }
    
    },
   {
      "crop":{
         "points":[
            34,
            56,
            67,
            87
         ],
         "zoom":1.45     
       }
    
    },
   "original",
   "optimize",
   {
      "subDirectory":"0-15"
    },
   {
      "imageQuality":"90"
    }
]
```
