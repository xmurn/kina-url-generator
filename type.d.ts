export interface UrlParams {
    domain: string
    clientId: string
    fileId: string | number,
    fileName: string,
    actions: (
        Resize
        | Crop
        | SubDirectory
        | ImageQuality
        | "original"
        | "optimize" )[]
}

export interface Resize {
    resize: {
        width?: string | number,
        height?: string | number,
        aspectRatio?: boolean,
        minSize?: boolean
    }
}

export interface Crop {
    crop: {
        points: String[] | Number[],
        zoom?: string | number
    }
}

export interface SubDirectory {
    subDirectory: string
}

export interface ImageQuality {
    imageQuality: string | number,
}
