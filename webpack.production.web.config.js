const path         = require('path')
const MinifyPlugin = require('babel-minify-webpack-plugin')

module.exports     = {
    entry  : path.resolve(__dirname, 'src/web.index.ts'),
    output : {
        path         : path.resolve(__dirname, 'dist/web'),
        filename     : 'KinaTools.js',
        library      : 'KinaTools',
        libraryTarget: 'umd'
    },
    module : {
        rules: [
            {
                test   : /\.tsx?$/,
                loader : 'ts-loader',
                exclude: /node_modules/,
            }
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
        modules   : [path.resolve(__dirname, 'src')],
    },
    mode   : 'production',
    devtool: 'source-map',
    plugins: [
        new MinifyPlugin({}, {})
    ]
}
