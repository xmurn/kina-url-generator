module.exports = {
    transform           : {
        '^.+\\.(t|j)sx?$': 'ts-jest'
    },
    testRegex           : '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
    moduleFileExtensions: [
        'ts',
        'js',
        'json',
        'node'
    ],
    coverageThreshold   : {
        global: {
            functions : 100,
            lines     : 100,
            statements: 100,
            branches  : 96.41
        }
    }
}

