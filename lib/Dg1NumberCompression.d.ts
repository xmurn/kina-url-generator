declare const Dg1NumberCompression: {
    dictionary: string;
    compress(numb: string | number): string;
    deCompress(code: string): number;
};
export { Dg1NumberCompression };
