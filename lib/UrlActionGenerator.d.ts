import * as UrlType from '../type';
declare const UrlActionGenerator: {
    addActions(actions: any): string;
    crop(data: UrlType.Crop): string;
    resize(data: UrlType.Resize): string;
    optimize(): string;
    original(): string;
    quality(data: UrlType.ImageQuality): string;
    subDirectory(data: UrlType.SubDirectory): string;
};
export { UrlActionGenerator };
