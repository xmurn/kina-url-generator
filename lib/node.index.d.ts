declare const KinaTools: {
    parseUrl: (url: String) => import("../type").UrlParams;
    createUrl: (urlParams: import("../type").UrlParams) => string;
    clientMapDef: {
        [index: string]: string;
    };
};
export default KinaTools;
