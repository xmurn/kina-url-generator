import * as UrlType from '../type';
declare const UrlParser: {
    getCropRegex(): RegExp;
    getResizeRegex(): RegExp;
    compressedParamsUrlVersion(urlParams: string[], parsedData: UrlType.UrlParams): void;
    siriusPlaceholderUrlVersion(urlParams: string[], parsedData: UrlType.UrlParams): void;
    fileManagerUrlVersion(urlParams: string[], parsedData: UrlType.UrlParams): void;
    noActionUrlVersion(urlParams: string[], parsedData: UrlType.UrlParams): void;
    getActionParameters(actionParameters: string[], parsedData: UrlType.UrlParams, postActionParameter: any): void;
    getRegexMatchGroups(parsedData: any, regex: any, actionData: string, actionName: string): void;
};
export { UrlParser };
