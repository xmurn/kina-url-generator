import * as UrlType from '../type';
/**
 * This utility will rewrite numbers with alphanumeric symbols.
 * Approx. savings are 40%.
 *
 * @type {{dictionary: string, compress: (function(*): string), deCompress: (function(*): number)}}
 */
declare const clientMapDef: {
    [index: string]: string;
};
declare const parseUrl: (url: String) => UrlType.UrlParams;
declare const createUrl: (urlParams: UrlType.UrlParams) => string;
export { parseUrl, createUrl, clientMapDef };
