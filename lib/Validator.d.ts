import * as UrlType from '../type';
declare const Validator: {
    validateData(url: UrlType.UrlParams): void;
    clientId(clientId: string): void;
    fileName(fileName: string): void;
    fileId(fileId: string | number): void;
    domain(domain: string): void;
    actions(actions: any): void;
    crop(cropParams: any): void;
    resize(resizeParams: any): void;
    subDirectory(subDirectoryParams: string): void;
    imageQuality(imageQualityParams: string): void;
};
export { Validator };
