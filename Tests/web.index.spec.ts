import {
    clientMapDef,
    createUrl,
    parseUrl
} from '../src/web.index'

describe('web.index.spec.ts', () => {
    describe('exportsOK', () => {
        it('parseUrl', () => {
            expect(parseUrl).toBeInstanceOf(Function)
        })
        it('createUrl', () => {
            expect(createUrl).toBeInstanceOf(Function)
        })
        it('clientMapDef', () => {
            expect(clientMapDef).toStrictEqual({"dg1": "d", "main": "d", "sirius": "s", "themes": "t", "themes-export": "te"})
        })
    })
})
