import {UrlParser} from '../src/UrlParser'

import * as UrlType from '../type'

describe('UrlParser.spec.ts', () => {
    let parsedData: UrlType.UrlParams;

    beforeEach(() => {
        parsedData = {
            domain  : '',
            clientId: '',
            fileId  : '',
            fileName: '',
            actions : []
        }
    })

    describe('compressedParamsUrlVersion', () => {
        it('Return data with tenant id', () => {
            const urlParams = [ 'https:', '', 'dj2kc30dfpf18.cloudfront.net', 'c', '1', '236', '6V-7J-xc-mx-1.81fJJFyEJ', 'shutterstock.jpg' ]
            UrlParser.compressedParamsUrlVersion(urlParams, parsedData)

            expect(parsedData).toStrictEqual({
                'actions': [
                    {
                        'crop': {
                            'points': [
                                429,
                                479,
                                2058,
                                1397
                            ],
                            'zoom': 1.1751136363636365
                        }
                    }
                ],
                'clientId': '1',
                'domain': 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId': 236,
                'fileName': 'shutterstock.jpg'
            })
        })
        it('Return data with dg1 client', () => {
            const urlParams = [ 'https:', '', 'dj2kc30dfpf18.cloudfront.net', 'c', 'd', '123', '6V-7J-xc-mx-1.81fJJFyEJ', 'shutterstock.jpg' ]
            UrlParser.compressedParamsUrlVersion(urlParams, parsedData)

            expect(parsedData).toStrictEqual({
                'actions': [
                    {
                        'crop': {
                            'points': [
                                429,
                                479,
                                2058,
                                1397
                            ],
                            'zoom': 1.1751136363636365
                        }
                    }
                ],
                'clientId': 'dg1',
                'domain': 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId': 123,
                'fileName': 'shutterstock.jpg'
            })
        })
    })
    describe('noActionUrlVersion', () => {
        it('Return valid data', () => {
            const urlParams = [ 'https:', '', 'dj2kc30dfpf18.cloudfront.net', 'c', 'themes-export', '448', 'visao_how_to_book.png' ]
            UrlParser.noActionUrlVersion(urlParams, parsedData)

            expect(parsedData).toStrictEqual({
                'actions': ['optimize'],
                'clientId': 'themes-export',
                'domain': 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId': 448,
                'fileName': 'visao_how_to_book.png'
            })
        })
    })
    describe('fileManagerUrlVersion', () => {
        it('Return valid data, multitenant', () => {
            const urlParams = [ 'https:', '', 'dj2kc30dfpf18.cloudfront.net', '6', '1', '82', 'rw34:cL34R32l12r65z1.23', 'q34', 'image.jpg' ]
            UrlParser.fileManagerUrlVersion(urlParams, parsedData)

            expect(parsedData).toStrictEqual({
                'actions': [{
                    resize: {
                        width: '34'
                    }
                }, {
                    crop: {
                        points: ['34', '32', '12', '65'],
                        zoom: '1.23'
                    }
                }, {
                    imageQuality: '34'
                }],
                'clientId': '1',
                'domain': 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId': 82,
                'fileName': 'image.jpg'
            })
        })
        it('Return valid data, themes-export', () => {
            const urlParams = [ 'https:', '', 'dj2kc30dfpf18.cloudfront.net', '6', 'te', '82', 'd3-4:o', 'image.jpg' ]
            UrlParser.fileManagerUrlVersion(urlParams, parsedData)

            expect(parsedData).toStrictEqual({
                'actions': [{
                    subDirectory: '3-4'
                }, 'optimize'],
                'clientId': 'themes-export',
                'domain': 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId': 82,
                'fileName': 'image.jpg'
            })
        })
    })
    describe('siriusPlaceholderUrlVersion', () => {
        it('Return valid data, multitenant', () => {
            const urlParams = [ 'https:', '', 'dj2kc30dfpf18.cloudfront.net', '6', 's', '0', 'rw34h43a1i0:cL34R32l12r65z1.23', 'q50', 'image.jpg' ]
            UrlParser.siriusPlaceholderUrlVersion(urlParams, parsedData)

            expect(parsedData).toStrictEqual({
                'actions': [{
                    resize: {
                        width: '34',
                        height: '43',
                        aspectRatio: true,
                        minSize: false
                    }
                }, {
                    crop: {
                        points: ['34', '32', '12', '65'],
                        zoom: '1.23'
                    }
                }, {
                    imageQuality: '50'
                }],
                'clientId': 'sirius',
                'domain': 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId': '',
                'fileName': 'image.jpg'
            })
        })
        it('Return valid data, themes-export', () => {
            const urlParams = [ 'https:', '', 'dj2kc30dfpf18.cloudfront.net', '6', 's', '0', 'n', 'image.jpg' ]
            UrlParser.siriusPlaceholderUrlVersion(urlParams, parsedData)

            expect(parsedData).toStrictEqual({
                'actions': ['original'],
                'clientId': 'sirius',
                'domain': 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId': '',
                'fileName': 'image.jpg'
            })
        })
        it('Unknown params', () => {
            const urlParams = [ 'https:', '', 'dj2kc30dfpf18.cloudfront.net', '6', 's', '0', 'r34:c23', 'r34', 'image.jpg' ]
            UrlParser.siriusPlaceholderUrlVersion(urlParams, parsedData)

            expect(parsedData).toStrictEqual({
                'actions': [],
                'clientId': 'sirius',
                'domain': 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId': '',
                'fileName': 'image.jpg'
            })
        })
    })
})
