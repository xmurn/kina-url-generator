import {UrlActionGenerator} from '../src/UrlActionGenerator'
describe('UrlActionGenerator.ts', () => {
    it('Add various actions', () => {
        expect(UrlActionGenerator.addActions([
            {
                resize: {
                    width      : 45,
                    height     : 65,
                    aspectRatio: 1,
                    minSize    : 0
                }
            },
            {
                crop: {
                    points: ['34', '23', '43', '28'],
                    zoom  : 2.3
                }
            },
            {
                imageQuality: '90'
            }
        ])).toEqual('rw45h65a1i0:cL34R23l43r28z2.3/q90')
    })
    it('No zoom parameter, only resize using width value', () => {
        expect(UrlActionGenerator.addActions([
            {
                resize: {
                    width      : '45'
                }
            },
            {
                crop: {
                    points: ['34', '23', '43', '28']
                }
            }
        ])).toEqual('rw45:cL34R23l43r28')
    })
    it('Only resize by height value', () => {
        expect(UrlActionGenerator.addActions([
            {
                resize: {
                    height      : 31
                }
            }
        ])).toEqual('rh31')
    })
    it('Optimize image with image quality parameter', () => {
        expect(UrlActionGenerator.addActions([
            'optimize',
            {
                imageQuality: '30'
            }
        ])).toEqual('o/q30')
    })
    it('Original image', () => {
        expect(UrlActionGenerator.addActions([
            'original'
        ])).toEqual('n')
    })
    it('No action parameters', () => {
        expect(UrlActionGenerator.addActions([])).toEqual('')
    })
    it('Not valid actions parameters', () => {
        expect(UrlActionGenerator.addActions([{ 'test': 234 }, 'test'])).toEqual('')
    })
})
