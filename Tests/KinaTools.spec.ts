import {parseUrl,createUrl}  from '../src/KinaTools'
import * as UrlType from '../type'

describe('KinaTools.spec.ts', () => {
    describe('parseUrl', () => {
        it('Not supported url version', () => {
            const url = 'https://dj2kc30dfpf18.cloudfront.net/c/image.jpg'

            expect(parseUrl(url)).toStrictEqual({
                'actions' : [],
                'clientId': '',
                'domain'  : '',
                'fileId'  : '',
                'fileName': ''
            })
        })
        it('Url version with compressed cropping params', () => {
            const url = 'https://dj2kc30dfpf18.cloudfront.net/c/1/236/6V-7J-xc-mx-1.81fJJFyEJ/shutterstock.jpg'

            expect(parseUrl(url)).toStrictEqual({
                'actions' : [
                    {
                        'crop': {
                            'points': [
                                429,
                                479,
                                2058,
                                1397
                            ],
                            'zoom'  : 1.1751136363636365
                        }
                    }
                ],
                'clientId': '1',
                'domain'  : 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId'  : 236,
                'fileName': 'shutterstock.jpg'
            })
        })
        it('Sirius placeholder url version', () => {
            const url = 'https://dj2kc30dfpf18.cloudfront.net/6/s/0/d1-9:cL45R34r22l65:rh56w23/test.jpg'

            expect(parseUrl(url)).toStrictEqual({
                'actions' : [
                    {
                        'subDirectory': '1-9'
                    },
                    {
                        'crop': {
                            'points': [
                                '45',
                                '34',
                                '22',
                                '65'
                            ],
                            'zoom'  : 1
                        }
                    },
                    {
                        'resize': {
                            'height': '56',
                            'width' : '23'
                        }
                    }
                ],
                'clientId': 'sirius',
                'domain'  : 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId'  : '',
                'fileName': 'test.jpg'
            })
        })
        it('File manager url version with image quality parameter', () => {
            const url = 'https://dj2kc30dfpf18.cloudfront.net/6/d/45/o/q100/image.png'

            expect(parseUrl(url)).toStrictEqual({
                'actions' : [
                    'optimize',
                    {
                        'imageQuality': '100'
                    }
                ],
                'clientId': 'dg1',
                'domain'  : 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId'  : 45,
                'fileName': 'image.png'
            })
        })
        it('File manager url version, original image', () => {
            const url = 'https://dj2kc30dfpf18.cloudfront.net/6/t/45/n/image.png'

            expect(parseUrl(url)).toStrictEqual({
                'actions' : [
                    'original'
                ],
                'clientId': 'themes',
                'domain'  : 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId'  : 45,
                'fileName': 'image.png'
            })
        })
        it('No action url version', () => {
            const url = 'https://dj2kc30dfpf18.cloudfront.net/c/themes-export/500/test.png'

            expect(parseUrl(url)).toStrictEqual({
                'actions' : [
                    'optimize'
                ],
                'clientId': 'themes-export',
                'domain'  : 'https://dj2kc30dfpf18.cloudfront.net',
                'fileId'  : 500,
                'fileName': 'test.png'
            })
        })
        it('Not supported version of url', () => {
            const url = 'https://dj2kc30dfpf18.cloudfront.net/23/432/8768/6542/test/353/image.jpg'

            expect(parseUrl(url)).toStrictEqual({
                'actions' : [],
                'clientId': '',
                'domain'  : '',
                'fileId'  : '',
                'fileName': ''
            })
        })
    })
    describe('createUrl', () => {
        it('clientId parameter is not valid', () => {
            const urlParams: UrlType.UrlParams = {
                'actions' : ['optimize'],
                'clientId': 'test',
                'domain'  : 'http://test',
                'fileId'  : '23',
                'fileName': 'image'
            }

            let error = ''
            try {
                createUrl(urlParams)
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Not valid clientId parameter')
        })
        it('empty actions parameter', () => {
            const urlParams: UrlType.UrlParams = {
                'actions' : [],
                'clientId': 'dg1',
                'domain'  : 'http://test',
                'fileId'  : '23',
                'fileName': 'image'
            }

            let error = ''
            try {
                createUrl(urlParams)
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Empty actions parameter')
        })
        it('Valid url parameters, client is dg1', () => {
            const urlParams: UrlType.UrlParams = {
                'actions' : [
                    {
                        resize: {
                            width: '45'
                        }
                    },
                    {
                        crop: {
                            points: [
                                '34',
                                '90',
                                '112',
                                '324'
                            ]
                        }
                    }
                ],
                'clientId': 'dg1',
                'domain'  : 'http://test',
                'fileId'  : '23',
                'fileName': 'image.jpg'
            }

            return expect(createUrl(urlParams)).toEqual('http://test/6/d/23/rw45:cL34R90l112r324/image.jpg')

        })
        it('Valid url parameters, client is tenant 45', () => {
            const urlParams: UrlType.UrlParams = {
                'actions' : [
                    {
                        subDirectory: '0-12'
                    },
                    {
                        imageQuality: '99'
                    }
                ],
                'clientId': '45',
                'domain'  : 'http://test',
                'fileId'  : '13',
                'fileName': 'image.jpg'
            }

            return expect(createUrl(urlParams)).toEqual('http://test/6/45/13/d0-12/q99/image.jpg')
        })
    })
})
