import KinaTools from '../src/node.index'
import {
    clientMapDef,
    createUrl,
    parseUrl
} from '../src/KinaTools'

describe('node.index.spec.ts', () => {
    describe('exportsOK', () => {
        it('KinaTools', () => {
            expect(KinaTools).toBeInstanceOf(Object)
        })
        it('parseUrl', () => {
            expect(KinaTools.parseUrl).toBeInstanceOf(Function)
        })
        it('createUrl', () => {
            expect(KinaTools.createUrl).toBeInstanceOf(Function)
        })
        it('clientMapDef', () => {
            expect(KinaTools.clientMapDef).toStrictEqual({"dg1": "d", "main": "d", "sirius": "s", "themes": "t", "themes-export": "te"})
        })
    })
})
