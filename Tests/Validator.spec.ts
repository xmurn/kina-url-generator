import {Validator} from '../src/Validator'

describe('Validator.spec.ts', () => {
    describe('clientId', () => {
        it('Not valid client parameter', () => {
            let error = ''
            try {
                Validator.clientId('')
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Not valid clientId parameter')
        })
        it('Valid client parameter', () => {
            let error = ''
            try {
                Validator.clientId('dg1')
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
    })
    describe('fileName', () => {
        it('Valid fileName parameter', () => {
            let error = ''
            try {
                Validator.fileName('image.jpg')
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
    })
    describe('fileId', () => {
        it('Valid fileId parameter', () => {
            let error = ''
            try {
                Validator.fileId('23')
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
    })
    describe('domain', () => {
        it('Valid domain parameter', () => {
            let error = ''
            try {
                Validator.domain('http://...')
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
    })
    describe('crop', () => {
        it('Empty crop parameters', () => {
            let error = ''
            try {
                Validator.crop({})
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Empty crop parameters')
        })
        it('Point values are not valid', () => {
            let error = ''
            try {
                Validator.crop({points: []})
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Point values are not valid')
        })
        it('Zoom parameter is not valid', () => {
            let error = ''
            try {
                Validator.crop({
                    points: [
                        '3',
                        '3',
                        2,
                        2
                    ],
                    zoom  : ''
                })
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Zoom parameter is not valid')
        })
        it('Not supported w parameter', () => {
            let error = ''
            try {
                Validator.crop({
                    w: [
                        '3',
                        '3',
                        2,
                        2
                    ]
                })
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Not supported crop parameter w')
        })
        it('Valid parameters', () => {
            let error = ''

            try {
                Validator.crop({
                    points: [
                        '3',
                        '3',
                        2,
                        2
                    ],
                    zoom  : '2.3'
                })
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
    })
    describe('resize', () => {
        it('Empty resize parameters', () => {
            let error = ''
            try {
                Validator.resize({})
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Empty resize parameters')
        })
        it('Resize parameters are not valid, width is NaN', () => {
            let error = ''
            try {
                Validator.resize({width: []})
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Resize parameters are not valid, width is NaN')
        })
        it('Resize parameters are not valid, height is NaN', () => {
            let error = ''
            try {
                Validator.resize({height: ''})
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Resize parameters are not valid, height is NaN')
        })
        it('Resize parameters are not valid, aspectRatio is not boolean type', () => {
            let error = ''
            try {
                Validator.resize({aspectRatio: ''})
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Resize parameters are not valid, aspectRatio is not boolean type')
        })
        it('Resize parameters are not valid, minSize is not boolean type', () => {
            let error = ''
            try {
                Validator.resize({minSize: ''})
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Resize parameters are not valid, minSize is not boolean type')
        })
        it('Not supported resize parameter l', () => {
            let error = ''
            try {
                Validator.resize({l: ''})
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Not supported resize parameter l')
        })
        it('Valid resize parameters', () => {
            let error = ''
            try {
                Validator.resize({
                    width      : 23,
                    height     : 45,
                    aspectRatio: true,
                    minSize    : false
                })
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
    })
    describe('subDirectory', () => {
        it('SubDirectory parameter is valid', () => {
            let error = ''
            try {
                Validator.subDirectory('3-45')
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
    })
    describe('imageQuality', () => {
        it('imageQuality parameter is valid', () => {
            let error = ''
            try {
                Validator.imageQuality('2')
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
    })
    describe('actions', () => {
        it('Empty actions parameter', () => {
            let error = ''
            try {
                Validator.actions([])
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Empty actions parameter')
        })
        it('Not supported action data', () => {
            let error = ''
            try {
                Validator.actions(['data'])
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Action parameter data is not supported')
        })
        it('Not supported object action data', () => {
            let error = ''
            try {
                Validator.actions([{data: {}}])
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Action parameter data is not supported')
        })
        it('Not supported object action data...', () => {
            let error = ''
            try {
                Validator.actions([{resize: {}}])
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Empty resize parameters')
        })
        it('Valid actions data', () => {
            let error = ''
            try {
                Validator.actions([
                    {
                        resize: {
                            width: '23'
                        }
                    },
                    {
                        crop: {
                            points: [
                                23,
                                45,
                                65,
                                45
                            ]
                        }
                    },
                    {
                        subDirectory: '1-0'
                    },
                    {
                        imageQuality: '80'
                    }
                ])
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
    })
    describe('validateData', () => {
        it('Data is valid', () => {
            let error = ''
            try {
                Validator.validateData({
                    clientId: 'dg1',
                    fileName: 'test',
                    fileId  : '45',
                    domain  : 'http://test',
                    actions : ['original']
                })
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('')
        })
        it('Empty actions parameter', () => {
            let error = ''
            try {
                Validator.validateData({
                    clientId: 'dg1',
                    fileName: 'test',
                    fileId  : '45',
                    domain  : 'http://test',
                    actions : []
                })
            } catch (err) {
                error = err.message
            }
            expect(error).toBe('Empty actions parameter')
        })
    })
})
