import {Dg1NumberCompression} from '../src/Dg1NumberCompression'

describe('Dg1NumberCompression.ts', () => {
    describe('Compress', () => {
        it('1223234234', () => {
            expect(Dg1NumberCompression.compress('1223234234')).toEqual('1kMz6q')
        })

        it('-8473', () => {
            expect(Dg1NumberCompression.compress('-8473')).toEqual('$2cF')
        })

        it('0.13231231', () => {
            expect(Dg1NumberCompression.compress('0.13231231')).toEqual('0.Tw2X')
        })

        it('-0.13231231', () => {
            expect(Dg1NumberCompression.compress('-0.13231231')).toEqual('$0.Tw2X')
        })

        it('-2.13231231', () => {
            expect(Dg1NumberCompression.compress('-2.13231231')).toEqual('$2.Tw2X')
        })

        it('1.6444', () => {
            expect(Dg1NumberCompression.compress('1.6444')).toEqual('1.1FW')
        })
    })

    describe('DeCompress', () => {
        it('1kMz6q', () => {
            expect(Dg1NumberCompression.deCompress('1kMz6q')).toEqual(1223234234)
        })

        it('$2cF', () => {
            expect(Dg1NumberCompression.deCompress('$2cF')).toEqual(-8473)
        })

        it('0.Tw2X', () => {
            expect(Dg1NumberCompression.deCompress('0.Tw2X')).toEqual(0.13231231)
        })

        it('$0.Tw2X', () => {
            expect(Dg1NumberCompression.deCompress('$0.Tw2X')).toEqual(-0.13231231)
        })

        it('$2.Tw2X', () => {
            expect(Dg1NumberCompression.deCompress('$2.Tw2X')).toEqual(-2.13231231)
        })

        it('1.1FW', () => {
            expect(Dg1NumberCompression.deCompress('1.1FW')).toEqual(1.6444)
        })
    })
})
