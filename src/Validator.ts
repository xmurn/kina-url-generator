import * as UrlType from '../type'

const Validator = {
    validateData(url: UrlType.UrlParams) {
        this.clientId(url.clientId)
        this.fileName(url.fileName)
        this.fileId(url.fileId)
        this.domain(url.domain)
        this.actions(url.actions)
    },
    clientId(clientId: string) {
        if (![
            'dg1',
            'main',
            'themes',
            'themes-export',
            'sirius'
        ].includes(clientId) && isNaN(parseInt(clientId))) {
            throw new Error('Not valid clientId parameter')
        }
    },
    fileName(fileName: string) {
        if (typeof fileName !== 'string') {
            /* istanbul ignore next */
            throw new Error('File name is not a string')
        }
    },
    fileId(fileId: string | number) {
        if (isNaN(parseInt(fileId.toString()))) {
            /* istanbul ignore next */
            throw new Error('File id must be a number')
        }
    },
    domain(domain: string) {
        if (typeof domain !== 'string') {
            /* istanbul ignore next */
            throw new Error('Domain parameter is not a string')
        }
    },
    actions(actions: any) {
        if (actions.length === 0) {
            throw new Error('Empty actions parameter')
        }

        for (const data of actions) {
            if (typeof data === 'object') {
                const action = Object.keys(data)
                if (action.length !== 1 || ![
                    'resize',
                    'crop',
                    'subDirectory',
                    'imageQuality'
                ].includes(action[0])) {
                    throw new Error(`Action parameter ${action[0]} is not supported`)
                } else if (action[0] === 'resize') {
                    this.resize(data.resize)
                } else if (action[0] === 'crop') {
                    this.crop(data.crop)
                } else if (action[0] === 'subDirectory') {
                    this.subDirectory(data.subDirectory)
                } else {
                    this.imageQuality(data.imageQuality)
                }
            } else if (typeof data !== 'string' || ![
                'optimize',
                'original'
            ].includes(data)) {
                throw new Error(`Action parameter ${data} is not supported`)
            }
        }
    },
    crop(cropParams: any) {
        const params = Object.keys(cropParams)

        if (params.length === 0) {
            throw new Error('Empty crop parameters')
        }

        for (const data of params) {
            if (data === 'points' && (typeof cropParams[data] !== 'object' || cropParams[data].length !== 4)) {
                throw new Error('Point values are not valid')
            } else if (data === 'zoom' && isNaN(parseInt(cropParams[data]))) {
                throw new Error('Zoom parameter is not valid')
            } else if (![
                'zoom',
                'points'
            ].includes(data)) {
                throw new Error(`Not supported crop parameter ${data}`)
            }
        }
    },
    resize(resizeParams: any) {
        const params = Object.keys(resizeParams)

        if (params.length === 0) {
            throw new Error('Empty resize parameters')
        }

        for (const data of params) {
            if (data === 'width' && isNaN(parseInt(resizeParams[data]))) {
                throw new Error('Resize parameters are not valid, width is NaN')
            } else if (data === 'height' && isNaN(parseInt(resizeParams[data]))) {
                throw new Error('Resize parameters are not valid, height is NaN')
            } else if (data === 'aspectRatio' && typeof resizeParams[data] !== 'boolean') {
                throw new Error('Resize parameters are not valid, aspectRatio is not boolean type')
            } else if (data === 'minSize' && typeof resizeParams[data] !== 'boolean') {
                throw new Error('Resize parameters are not valid, minSize is not boolean type')
            } else if (![
                'width',
                'height',
                'aspectRatio',
                'minSize'
            ].includes(data)) {
                throw new Error(`Not supported resize parameter ${data}`)
            }
        }
    },
    subDirectory(subDirectoryParams: string) {
        if (typeof subDirectoryParams !== 'string') {
            /* istanbul ignore next */
            throw new Error('SubDirectory parameter is not valid')
        }
    },
    imageQuality(imageQualityParams: string) {
        if (isNaN(parseInt(imageQualityParams)) || parseInt(imageQualityParams) > 100) {
            /* istanbul ignore next */
            throw new Error('Image quality parameter is not valid')
        }
    }
}

export {Validator}
