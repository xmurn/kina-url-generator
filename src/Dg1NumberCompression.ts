const Dg1NumberCompression = {
    dictionary: '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

    compress(numb: string | number) {

        let result      = ''
        let num         = parseFloat(numb.toString())
        let wasNegative = false
        if (num < 0) {
            // Make number positive
            num         = num * (-1)
            wasNegative = true
        }

        if (Math.floor(num) !== num) {

            const n       = num.toString()
            const numbers = n.split('.')

            // Handle decimal values
            result = this.compress(parseInt(numbers[0], 10)) + '.'
            result += this.compress(parseInt(numbers[1], 10))

        } else {

            if (num === 0) {
                result = this.dictionary.charAt(num)
            }

            const l = this.dictionary.length
            while (num) {
                result = this.dictionary.charAt(num % l) + result
                num    = Math.floor(num / l)
            }
        }

        if (wasNegative) {
            result = '$' + result
        }

        return result
    },

    deCompress(code: string) {

        let result = 0

        let wasNegative = false
        if (code.indexOf('$') === 0) {
            wasNegative = true
            code        = code.replace('$', '')
        }

        if (code.indexOf('.') !== -1) {
            // Code is a representation of decimal number with $ as a decimal separator

            const codes = code.split('.')

            // Decode decimal values
            let decimalResult = this.deCompress(codes[0]) + '.'
            decimalResult += this.deCompress(codes[1])

            result = parseFloat(decimalResult)

        } else {

            const cl = code.length
            for (let i = 0; i < cl; i++) {
                result = (result * this.dictionary.length + this.dictionary.indexOf(code.charAt(i)))
            }
        }

        if (wasNegative) {
            result = result * (-1)
        }

        return result
    }
}

export {Dg1NumberCompression}
