import {
    clientMapDef,
    createUrl,
    parseUrl
} from './KinaTools'

const KinaTools = {
    parseUrl,
    createUrl,
    clientMapDef
}
export default KinaTools
