import * as UrlType from '../type'

const UrlActionGenerator   = {
    addActions(actions: any) {
        let actionsUrl        = ''
        let postActionsUrl    = ''
        let subDirectoryExist = false

        for (const data of actions) {
            if (typeof data === 'object') {
                const action = Object.keys(data)[0]

                if (action === 'imageQuality') {
                    postActionsUrl += ':' + this.quality(data)
                } else if (action === 'resize') {
                    actionsUrl += ':' + this.resize(data)
                } else if (action === 'crop') {
                    actionsUrl += ':' + this.crop(data)
                } else if (action === 'subDirectory') {
                    actionsUrl += ':' + this.subDirectory(data)
                    subDirectoryExist = true
                }
            } else if (!subDirectoryExist) {
                actionsUrl = data === 'optimize' ? this.optimize() : data === 'original' ? this.original() : ''
            }
        }

        if (actionsUrl.startsWith(':')) {
            actionsUrl = actionsUrl.substr(1)
        }

        if (postActionsUrl.startsWith(':')) {
            postActionsUrl = postActionsUrl.substr(1)
            actionsUrl     = actionsUrl + '/' + postActionsUrl
        }

        return actionsUrl
    },
    crop(data: UrlType.Crop) {
        return 'c' + 'L' + data.crop.points[0] + 'R' + data.crop.points[1] + 'l' + data.crop.points[2] + 'r' + data.crop.points[3]
            + (typeof data.crop.zoom !== 'undefined' ? 'z' + data.crop.zoom : '')
    },
    resize(data: UrlType.Resize) {
        return 'r' + (typeof data.resize.width !== 'undefined' ? 'w' + data.resize.width : '')
            + (typeof data.resize.height !== 'undefined' ? 'h' + data.resize.height : '')
            + (typeof data.resize.aspectRatio !== 'undefined' ? 'a' + data.resize.aspectRatio : '')
            + (typeof data.resize.minSize !== 'undefined' ? 'i' + data.resize.minSize : '')
    },
    optimize() {
        return 'o'
    },
    original() {
        return 'n'
    },
    quality(data: UrlType.ImageQuality) {
        return 'q' + data.imageQuality
    },
    subDirectory(data: UrlType.SubDirectory) {
        return 'd' + data.subDirectory
    }
}

export {UrlActionGenerator}
