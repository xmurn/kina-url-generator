import * as UrlType           from '../type'
import {Dg1NumberCompression} from './Dg1NumberCompression'
import {clientMapDef}                 from './KinaTools'

const UrlParser            = {
    getCropRegex() {
        return /(L[0-9]+)|(R[0-9]+)|(l[0-9]+)|(r[0-9]+)|(z[0-9]*\.?[0-9]*)/gm
    },
    getResizeRegex() {
        return /(w[0-9]+)|(h[0-9]+)|(a[0-1])|(i[0-1])/gm
    },
    // Example of url: https://dj2kc30dfpf18.cloudfront.net/c/1/236/6V-7J-xc-mx-1.81fJJFyEJ/shutterstock.jpg
    compressedParamsUrlVersion(urlParams: string[], parsedData: UrlType.UrlParams) {
        const cropParams: UrlType.Crop = {
            crop: {
                points: [],
                zoom  : 1
            }
        }

        const compressedParams = urlParams[urlParams.length - 2].split('-')
        parsedData.actions.push(cropParams)

        // Decode old compressed cropping params
        for (let i = 0; i <= 3; i++) {
            // Decode each point and add it to array
            let point = Dg1NumberCompression.deCompress(compressedParams[i])
            //@ts-ignore
            parsedData.actions[0].crop.points.push(point)
        }
        // Decode zoom parameter
        //@ts-ignore
        parsedData.actions[0].crop.zoom = Dg1NumberCompression.deCompress(compressedParams[4])

        parsedData.domain   = urlParams.slice(0, 3).join('/')
        parsedData.fileId   = parseInt(urlParams[5], 10)
        parsedData.fileName = urlParams[urlParams.length - 1]

        const clientId = Object.keys(clientMapDef).find(key => clientMapDef[key] === urlParams[4])
        if (typeof clientId !== 'undefined') {
            //@ts-ignore
            parsedData.clientId = clientId
        } else {
            parsedData.clientId = urlParams[4]
        }
    },
    // Example of url: https://dj2kc30dfpf18.cloudfront.net/6/3/23/rw34/visao_how_to_book.png
    siriusPlaceholderUrlVersion(urlParams: string[], parsedData: UrlType.UrlParams) {
        parsedData.domain   = urlParams.slice(0, 3).join('/')
        parsedData.fileName = urlParams[urlParams.length - 1]
        parsedData.clientId = 'sirius'

        const postActionParameter = urlParams.length === 9 ? urlParams[7] : false

        this.getActionParameters(urlParams[6].split(':'), parsedData, postActionParameter)
    },
    // Example of url: https://dj2kc30dfpf18.cloudfront.net/6/s/0/d1-0:rw23/visao_how_to_book.png
    fileManagerUrlVersion(urlParams: string[], parsedData: UrlType.UrlParams) {
        parsedData.domain   = urlParams.slice(0, 3).join('/')
        parsedData.fileId   = parseInt(urlParams[5], 10)
        parsedData.fileName = urlParams[urlParams.length - 1]

        const postActionParameter = urlParams.length === 9 ? urlParams[7] : false

        this.getActionParameters(urlParams[6].split(':'), parsedData, postActionParameter)

        const clientId = Object.keys(clientMapDef).find(key => clientMapDef[key] === urlParams[4])

        if (typeof clientId !== 'undefined') {
            //@ts-ignore
            parsedData.clientId = clientId
        } else {
            parsedData.clientId = urlParams[4]
        }
    },
    // Example of url: https://dj2kc30dfpf18.cloudfront.net/c/themes-export/448/visao_how_to_book.png
    noActionUrlVersion(urlParams: string[], parsedData: UrlType.UrlParams) {
        parsedData.domain   = urlParams.slice(0, 3).join('/')
        parsedData.fileId   = parseInt(urlParams[5], 10)
        parsedData.fileName = urlParams[urlParams.length - 1]
        parsedData.clientId = urlParams[4]
        parsedData.actions  = ['optimize'] // TODO: check the value
    },
    getActionParameters(actionParameters: string[], parsedData: UrlType.UrlParams, postActionParameter: any) {
        for (const action of actionParameters) {
            switch (action.charAt(0)) {
                case 'd':
                    parsedData.actions.push({subDirectory: action.slice(1)})
                    break
                case 'o':
                    parsedData.actions.push('optimize')
                    break
                case 'n':
                    parsedData.actions.push('original')
                    break
                case 'r':
                    let resizeData = {
                        resize: {}
                    }

                    this.getRegexMatchGroups(resizeData.resize, this.getResizeRegex(), action, 'resize')

                    if (Object.keys(resizeData.resize).length > 0) {
                        parsedData.actions.push(resizeData)
                    }
                    break
                case 'c':
                    let cropData: UrlType.Crop = {
                        crop: {
                            points: [],
                            zoom  : 1
                        }
                    }

                    this.getRegexMatchGroups(cropData.crop, this.getCropRegex(), action, 'crop')

                    if (cropData.crop.points.length > 0) {
                        parsedData.actions.push(cropData)
                    }
                    break
            }
        }

        if (postActionParameter) {
            for (const action of postActionParameter.split(':')) {
                if (action.startsWith('q')) {
                    parsedData.actions.push({imageQuality: action.slice(1)})
                }
            }
        }
    },
    getRegexMatchGroups(parsedData: any, regex: any, actionData: string, actionName: string) {
        let m

        while ((m = regex.exec(actionData)) !== null) {
            // This is necessary to avoid infinite loops with zero-width matches
            /* istanbul ignore if */
            if (m.index === regex.lastIndex) {
                regex.lastIndex++
            }

            // The result can be accessed through the `m`-variable.
            m.forEach((match: undefined | string, groupIndex: number) => {
                if (match !== undefined && actionName === 'resize') {
                    if (groupIndex === 1) {
                        parsedData['width'] = match.slice(1)
                    } else if (groupIndex === 2) {
                        parsedData['height'] = match.slice(1)
                    } else if (groupIndex === 3) {
                        parsedData['aspectRatio'] = match.slice(1) === '1'
                    } else if (groupIndex === 4) {
                        parsedData['minSize'] = match.slice(1) === '1'
                    }
                    /* istanbul ignore else */
                } else if (match !== undefined && actionName === 'crop') {
                    if (groupIndex === 1 || groupIndex === 2 || groupIndex === 3 || groupIndex === 4) {
                        parsedData['points'].push(match.slice(1))
                    } else if (groupIndex === 5) {
                        parsedData['zoom'] = match.slice(1)
                    }
                }
            })
        }
    }
}
export {UrlParser}
