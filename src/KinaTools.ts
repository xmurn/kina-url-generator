import * as UrlType         from '../type'
import {UrlParser}          from './UrlParser'
import {Validator}          from './Validator'
import {UrlActionGenerator} from './UrlActionGenerator'

/**
 * This utility will rewrite numbers with alphanumeric symbols.
 * Approx. savings are 40%.
 *
 * @type {{dictionary: string, compress: (function(*): string), deCompress: (function(*): number)}}
 */

const clientMapDef: { [index: string]: string } = {
    'themes'       : 't',
    'themes-export': 'te',
    'dg1'          : 'd',
    'main'         : 'd',
    'sirius'       : 's'
}

const parseUrl = (url: String) => {
    const params                        = url.split('/')
    const parsedData: UrlType.UrlParams = {
        domain  : '',
        clientId: '',
        fileId  : '',
        fileName: '',
        actions : []
    }

    if (params.length < 6) {
        return parsedData
    }

    if (params[params.length - 2].split('-').length === 5) {
        UrlParser.compressedParamsUrlVersion(params, parsedData)
    } else if (params[4] === 's') {
        UrlParser.siriusPlaceholderUrlVersion(params, parsedData)
    } else if (params.length === 8 || params.length === 9) {
        UrlParser.fileManagerUrlVersion(params, parsedData)
    } else if (params.length === 7) {
        UrlParser.noActionUrlVersion(params, parsedData)
    }

    return parsedData
}

const createUrl = (urlParams: UrlType.UrlParams) => {
    const version = 6
    Validator.validateData(urlParams)

    let tenantId = urlParams.clientId
    if (isNaN(parseInt(urlParams.clientId))) {
        tenantId = clientMapDef[urlParams.clientId]
    }
    const actionParams = UrlActionGenerator.addActions(urlParams.actions)

    return `${urlParams.domain}/${version}/${tenantId}/${urlParams.fileId}/${actionParams}/${urlParams.fileName}`

}
export {
    parseUrl,
    createUrl,
    clientMapDef
}
