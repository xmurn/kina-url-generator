const path         = require('path')

module.exports     = {
    entry  : path.resolve(__dirname, 'src/index.ts'),
    output : {
        path         : path.resolve(__dirname, 'dist'),
        filename     : 'KinaTools.js',
        library      : 'KinaTools',
        libraryTarget: 'umd',
    },
    module : {
        rules: [
            {
                test   : /\.tsx?$/,
                loader : 'ts-loader',
                exclude: /node_modules/,
            }
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
        modules   : [path.resolve(__dirname, 'src')],
    },
    mode   : 'development',
    devtool: 'source-map'
}
